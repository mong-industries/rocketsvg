//
//  SVGParser.swift
//  rocketSVG
//
//  Created by Michael Ong on 24/9/18.
//  Copyright © 2018 mong Industries. All rights reserved.
//

import Foundation

public class SVGParser: NSObject, XMLParserDelegate {

    public enum Errors: Error {
        case cannotParse(reason: String)

        public var localizedDescription: String {
            switch self {
            case .cannotParse(let reason):
                return "Cannot parse SVG file reason: \(reason)"
            }
        }
    }

    public init?(data: Data) throws {

        super.init()

        let parser = XMLParser(data: data)
            parser.delegate = self

        if !parser.parse(), let error = parser.parserError {
            throw SVGParser.Errors.cannotParse(reason: error.localizedDescription)
        }
    }

    public func generateRenderList() -> [PSVGRenderListItemType] {
        return []
    }
}

extension SVGParser {

    public func parserDidStartDocument(_ parser: XMLParser) {

    }

    public func parserDidEndDocument(_ parser: XMLParser) {

    }


    public func parser(_ parser: XMLParser, foundCDATA CDATABlock: Data) {

    }

    public func parser(_ parser: XMLParser, foundComment comment: String) {

    }

    public func parser(_ parser: XMLParser, foundCharacters string: String) {

    }


    public func parser(_ parser: XMLParser, didStartMappingPrefix prefix: String, toURI namespaceURI: String) {

    }

    public func parser(_ parser: XMLParser, didEndMappingPrefix prefix: String) {

    }


    public func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {

    }

    public func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {

    }
}
