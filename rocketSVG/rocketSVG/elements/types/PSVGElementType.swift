//
//  PSVGElementType.swift
//  rocketSVG
//
//  Created by Michael Ong on 24/9/18.
//  Copyright © 2018 mong Industries. All rights reserved.
//

import Foundation

protocol PSVGElementType {

    var tag: String { get }
    var attributes: [PSVGElementAttributeType] { get }

    var allowedChildElements: [PSVGElementType] { get }
}
