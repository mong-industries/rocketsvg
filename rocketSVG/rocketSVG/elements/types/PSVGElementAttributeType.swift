//
//  PSVGElementAttributeType.swift
//  rocketSVG
//
//  Created by Michael Ong on 24/9/18.
//  Copyright © 2018 mong Industries. All rights reserved.
//

import Foundation

protocol PSVGElementAttributeType {

    var key: String { get }
    var allowableValues: [AnyClass] { get }
}

struct PSVGElementAttribute: PSVGElementAttributeType {

    let key: String
    let allowableValues: [AnyClass]

    init(key: String, allowableValues: [AnyClass]) {
        self.key = key
        self.allowableValues = allowableValues
    }
}
