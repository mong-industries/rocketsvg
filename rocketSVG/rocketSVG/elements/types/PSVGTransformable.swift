//
//  PSVGTransformable.swift
//  rocketSVG
//
//  Created by Michael Ong on 25/9/18.
//  Copyright © 2018 mong Industries. All rights reserved.
//

import Foundation
import QuartzCore

protocol PSVGAnimatedTransformListType {
    var baseVal: [CATransform3D] { get }
    var animVal: [CATransform3D] { get }
}

protocol PSVGTransformableType {

    var transform: PSVGAnimatedTransformListType { get }
}
